TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    parameterhandler.cpp

unix:!macx: LIBS += -linpm -lcurl -larchive -lboost_filesystem -lpugixml -lsqlite3

HEADERS += \
    parameterhandler.h
