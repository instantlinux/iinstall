#ifndef PARAMETERHANDLER_H
#define PARAMETERHANDLER_H

#include <libinpm/package.h>
#include <libinpm/repository.h>
#include <libinpm/database.h>
#include <libinpm/source.h>
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

class ParameterHandler
{
public:
    ParameterHandler();
    static bool install(int argc, char* argv[]);
    static bool installlocal(int argc, char* argv[]);
    static bool remove(int argc, char* argv[]);
    static bool compile(int argc, char* argv[]);
    static bool createrepo(int argc, char* argv[]);
    static bool upgrade();
    static bool help();
    static bool list();
    static bool installdependencies(std::string dependencies);
    static bool checkdependencies(std::string dependencies);
    static bool search(int argc, char* argv[]);
private:
    static bool unpack(const char *filename);
};

#endif // PARAMETERHANDLER_H
