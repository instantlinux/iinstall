#include <iostream>
#include "parameterhandler.h"

using namespace std;

int main(int argc, char* argv[]) {
    if (argc == 1) {
        cout << "\033[1;31mERROR \033[0m\033[1;37m- Usage: \033[0m" << endl;
        ParameterHandler::help();
        return 1;
    }

    string function = argv[1];

    if (getuid() != 0 && function != "-c" && function != "--compile") {
        cout << "\033[1;31mERROR \033[0m\033[1;37m- Please run iinstall with root priviliges! \033[0m" << endl;
        return 1;
    }
    if (getuid() == 0 && (function == "-c" || function == "--compile")) {
        cout << "\033[1;31mERROR \033[0m\033[1;37m- Please don't run 'iinstall -c/--compile' with root priviliges! \033[0m" << endl;
        return 1;
    }

    if (function == "-r" || function == "--remove") {
        return ParameterHandler::remove(argc, argv);
    } else if (function == "-u" || function == "--upgrade") {
        return ParameterHandler::upgrade();
    } else if (function == "-c" || function == "--compile") {
        return ParameterHandler::compile(argc, argv);
    } else if (function == "-C" || function == "--createrepo") {
        return ParameterHandler::createrepo(argc, argv);
    } else if (function == "-s" || function == "--search") {
        return ParameterHandler::search(argc, argv);
    } else if (function == "-l" || function == "--list") {
        return ParameterHandler::list();
    } else if (function == "-i" || function == "--install-local") {
        return ParameterHandler::installlocal(argc, argv);
    } else if (function == "-h" || function == "--help") {
        cout << "\33[1;32mUsage:\033[0m" << endl;
        return ParameterHandler::help();
    } else {
        return ParameterHandler::install(argc, argv);
    }

    return 1;
}
