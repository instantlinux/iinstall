#include "parameterhandler.h"
#include <boost/algorithm/string.hpp>
#include <fstream>
#include <sstream>
#include <pugixml.hpp>

using namespace std;

ParameterHandler::ParameterHandler() {
}

bool ParameterHandler::install(int argc, char* argv[]) {
    Database db;
    db.openfile();

    vector<Package> pkgstoinstall;

    for (Repository repo: db.listRepositories()) {
        if (!repo.downloadRepo()) {
            cout << "\033[1;31mERROR \033[0m\033[1;37m- Cannot download repo, please try again later! \033[0m" << endl;
            return false;
        }
        for (Package pkg: repo.getAllPackages()) {
            for(int i = 1; i < argc; i++) {
                if (pkg.getname() == argv[i]) {
                    pkgstoinstall.push_back(pkg);
                }
            }
        }
    }

    bool found = false;
    for (int i = 1; i < argc; i++) {
        for(Package pkg: pkgstoinstall) {
            if (pkg.getname() == argv[i]) found = true;
        }
        if (!found) {
            cout << "\033[1;31mERROR \033[0m\033[1;37m- Cannot find package \"" << argv[i] << "\"!\033[0m" << endl;
            return 1;
        }
        found = false;
    }

    cout << "\33[1;32mInstalling packages:";

    for (Package pkg: pkgstoinstall) {
        cout << " " << pkg.getname();
    }

    cout << "\033[0m" << endl << endl;

    for (Package pkg: pkgstoinstall) {
        if (pkg.getdependencies() != "") installdependencies(pkg.getdependencies());
        cout << "\033[1;37mInstalling package " << pkg.getname() << "...\033[0m" << endl;
        db.install(&pkg);
        cout << endl;
    }

    return true;
}

bool ParameterHandler::installdependencies(string dependencies) {
    Database db;
    db.openfile();

    vector<Package> depstoinstall;
    vector<string> depnames;
    boost::split(depnames, dependencies, boost::is_any_of(","));

    for (Repository repo: db.listRepositories()) {
        if (!repo.downloadRepo()) {
            cout << "\033[1;31mERROR \033[0m\033[1;37m- Cannot download repo, please try again later! \033[0m" << endl;
            return false;
        }
        for (Package pkg: repo.getAllPackages()) {
            for(string currentDep: depnames) {
                if (pkg.getname() == currentDep) {
                    depstoinstall.push_back(pkg);
                }
            }
        }
    }

    bool found = false;
    for (string currentDep: depnames) {
        for(Package pkg: depstoinstall) {
            if (pkg.getname() == currentDep) found = true;
        }
        if (!found) {
            cout << "\033[1;31mERROR \033[0m\033[1;37m- Cannot resolve dependency: \"" << currentDep << "\"!\033[0m" << endl;
            return 1;
        }
        found = false;
    }

    for (Package pkg: depstoinstall) {
        cout << "\033[1;37mInstalling dependency " << pkg.getname() << "...\033[0m" << endl;
        if (pkg.getdependencies() != "") installdependencies(pkg.getdependencies());
        db.install(&pkg);
        cout << endl;
    }

    return true;
}

bool ParameterHandler::checkdependencies(string dependencies) {
    ifstream file("/usr/share/inpm/db.sqlite");
    if (file.good()) {
        int rc;
        sqlite3 *db;
        rc = sqlite3_open("/usr/share/inpm/db.sqlite", &db);
        if (rc != SQLITE_OK) return false;

        sqlite3_stmt *statement;
        sqlite3_prepare_v2(db, "SELECT name, path, version, description, image, license, dependencies FROM pkgs", -1, &statement, NULL);

        vector<string> installedpkgs;

        while ((rc = sqlite3_step(statement)) == SQLITE_ROW) {
            installedpkgs.push_back(string(reinterpret_cast<const char *>(sqlite3_column_text(statement, 0))));
        }

        sqlite3_finalize(statement);

        vector<string> depstocheck;
        vector<string> depnames;
        boost::split(depnames, dependencies, boost::is_any_of(","));

        for (string pkg: installedpkgs) {
            for(string currentDep: depnames) {
                if (pkg == currentDep) {
                    depstocheck.push_back(pkg);
                }
            }
        }

        bool found = false;
        for (string currentDep: depnames) {
            for(string pkg: depstocheck) {
                if (pkg == currentDep) found = true;
            }
            if (!found) {
                cout << "\033[1;31mERROR \033[0m\033[1;37m- Dependency not installed: \"" << currentDep << "\"!\033[0m" << endl;
                return false;
            }
            found = false;
        }
    } else {
        cout << "\033[1;31mERROR \033[0m\033[1;37m- Cannot open database!\033[0m" << endl;
        return false;
    }

    return true;
}

bool ParameterHandler::remove(int argc, char* argv[]) {
    Database db;
    db.openfile();

    vector<Package> pkgstoremove;

    for (Package pkg: db.listInstalledPkgs()) {
        for(int i = 2; i < argc; i++) {
            if (pkg.getname() == argv[i]) {
                pkgstoremove.push_back(pkg);
            }
        }
    }

    bool found = false;
    for (int i = 2; i < argc; i++) {
        for(Package pkg: pkgstoremove) {
            if (pkg.getname() == argv[i]) found = true;
        }
        if (!found) {
            cout << "\033[1;31mERROR \033[0m\033[1;37m- Package not installed: \"" << argv[i] << "\"!\033[0m" << endl;
            return 1;
        }
        found = false;
    }

    cout << "\33[1;32mRemoving packages:";

    for (Package pkg: pkgstoremove) {
        cout << " " << pkg.getname();
    }

    cout << "\033[0m" << endl << endl;

    for (Package pkg: pkgstoremove) {
        cout << "\033[1mRemoving package " << pkg.getname() << "...\033[0m" << endl;
        db.uninstall(&pkg);
        cout << endl;
    }

    return true;
}

bool ParameterHandler::installlocal(int argc, char* argv[]) {
    Database db;
    db.openfile();

    if (argc != 4) {
        cout << "\33[1;32mUsage:\033[0m" << endl;
        return help();
    }

    Package pkg = Package(argv[2], argv[3]);

    cout << "\33[1;32mInstalling packages: " << pkg.getname() << "\033[0m" << endl << endl;

    if (pkg.getdependencies() != "") installdependencies(pkg.getdependencies());
    cout << "\033[1;37mInstalling package " << pkg.getname() << "...\033[0m" << endl;
    db.install(&pkg);
    cout << endl;

    return true;
}

bool ParameterHandler::compile(int argc, char* argv[]) {
    if (argc != 4) {
        cout << "\33[1;32mUsage:\033[0m" << endl;
        return help();
    }

    char cwd[1024];
    if (getcwd(cwd, sizeof(cwd)) == NULL) {
        cout << "\033[1;31mERROR \033[0m\033[1;37m- Cannot get current directory! \033[0m" << endl;
        return false;
    }
    string currentDir = cwd;
    Source src = Source(argv[3], currentDir + "/" + argv[2] + ".inp", argv[2]);
    src.extractarchive();

    string dependencies = src.getDependencies();
    if (dependencies != "") {
        if (checkdependencies(dependencies) == false) {
            return false;
        }
    }

    src.buildpackage();
    src.compress();

    return true;
}

bool ParameterHandler::createrepo(int argc, char* argv[]) {
    if (argc < 3) {
        cout << "\33[1;32mUsage:\033[0m" << endl;
        return help();
    }

    char buff[FILENAME_MAX];
    getcwd(buff, FILENAME_MAX);
    string workingdir(buff);

    vector<Package> pkgs;

    cout << "\033[1;37mReading packages...\033[0m" << endl;

    for(int i = 2; i < argc; i++) {
        ifstream file(argv[i]);
        if (!file.good()) {
            cout << "\033[1;31mERROR \033[0m\033[1;37m- Package not found: \"" << argv[i] << "\"! \033[0m" << endl;
            return false;
        }

        stringstream filename(argv[i]);
        string nameoffile = argv[i];
        string pkgname;
        getline(filename, pkgname, '.');
        cout << "    \33[1;32m->\033[0m Package: \"" << pkgname << "\"" << endl;
        ifstream source(nameoffile, std::ios::binary);
        ofstream out   ("/tmp/workingpkg.tar.gz", std::ios::binary);
        out << source.rdbuf();
        out.close();
        chdir("/tmp");
        unpack("/tmp/workingpkg.tar.gz");

        const char *foldername = ("/tmp/" + pkgname).c_str();
        struct stat sb;
        if (stat(foldername, &sb) == 0 && S_ISDIR(sb.st_mode)) {
            pugi::xml_document doc;
            pugi::xml_parse_result result = doc.load_file(("/tmp/" + pkgname + "/info").c_str());
            if (!result) {
                cout << "\033[1;31mERROR \033[0m\033[1;37m- Cannot read info-file! \033[0m" << endl;
                std::remove("/tmp/workingpkg.tar.gz");
                return false;
            }

            for (pugi::xml_node child: doc.children()) {
                pkgs.push_back(Package(child.child_value("url"),
                                       child.child_value("name"),
                                       child.child_value("version"),
                                       child.child_value("description"),
                                       child.child_value("image"),
                                       child.child_value("license"),
                                       child.child_value("dependencies")));
            }

            std::remove("/tmp/workingpkg.tar.gz");
        } else {
            cout << "\033[1;31mERROR \033[0m\033[1;37m- The name of the archive has to be PACKAGENAME.inp! \033[0m" << endl;
            std::remove("/tmp/workingpkg.tar.gz");
            return false;
        }
        chdir(workingdir.c_str());
    }

    cout << "\033[1;37mCreating repository...\033[0m" << endl;

    string finalXml = "<?xml version=\"1.0\" ?>";
    for (Package pkg: pkgs) {
        finalXml += "\n<" + pkg.getname() + ">";
        finalXml += "\n    <name>" + pkg.getname() + "</name>";
        finalXml += "\n    <url>" + pkg.geturl() + "</url>";
        finalXml += "\n    <version>" + pkg.getversion() + "</version>";
        finalXml += "\n    <description>" + pkg.getdescription() + "</description>";
        finalXml += "\n    <dependencies>" + pkg.getdependencies() + "</dependencies>";
        finalXml += "\n    </image>" + pkg.getimage() + "</image>";
        finalXml += "\n    <license>" + pkg.getlicense() + "</license>";
        finalXml += "\n</" + pkg.getname() + ">";
    }

    finalXml += "\n";

    std::ofstream file("repo.inrepo");
    file << finalXml;
    file.close();

    return true;
}

static int copy_data(struct archive *ar, struct archive *aw) {
    int r;
    const void *buff;
    size_t size;
    la_int64_t offset;

    for (;;) {
        r = archive_read_data_block(ar, &buff, &size, &offset);
        if (r == ARCHIVE_EOF)
            return (ARCHIVE_OK);
        if (r < ARCHIVE_OK)
            return (r);
        r = archive_write_data_block(aw, buff, size, offset);
        if (r < ARCHIVE_OK) {
            fprintf(stderr, "%s\n", archive_error_string(aw));
            return (r);
        }
    }
}

bool ParameterHandler::unpack(const char *filename) {
    struct archive *a;
    struct archive *ext;
    struct archive_entry *entry;
    int flags;
    int r;

    flags = ARCHIVE_EXTRACT_TIME;
    flags |= ARCHIVE_EXTRACT_PERM;
    flags |= ARCHIVE_EXTRACT_ACL;
    flags |= ARCHIVE_EXTRACT_FFLAGS;

    a = archive_read_new();
    archive_read_support_format_all(a);
    archive_read_support_compression_all(a);
    ext = archive_write_disk_new();
    archive_write_disk_set_options(ext, flags);
    archive_write_disk_set_standard_lookup(ext);
    if ((r = archive_read_open_filename(a, filename, 10240)))
        exit(1);
    for (;;) {
        r = archive_read_next_header(a, &entry);
        if (r == ARCHIVE_EOF)
            break;
        if (r < ARCHIVE_OK)
            fprintf(stderr, "%s\n", archive_error_string(a));
        if (r < ARCHIVE_WARN) {
            cout << "ARCHIVE WARNING" << endl;
            return false;
        }
        r = archive_write_header(ext, entry);
        if (r < ARCHIVE_OK)
            fprintf(stderr, "%s\n", archive_error_string(ext));
        else if (archive_entry_size(entry) > 0) {
            r = copy_data(a, ext);
            if (r < ARCHIVE_OK)
                fprintf(stderr, "%s\n", archive_error_string(ext));
            if (r < ARCHIVE_WARN) {
                cout << "ARCHIVE WARNING!" << endl;
                return false;
            }
        }
        r = archive_write_finish_entry(ext);
        if (r < ARCHIVE_OK)
            fprintf(stderr, "%s\n", archive_error_string(ext));
        if (r < ARCHIVE_WARN) {
            cout << "ARCHIVE WARNING!!!" << endl;
            return false;
        }
    }
    archive_read_close(a);
    archive_read_free(a);
    archive_write_close(ext);
    archive_write_free(ext);
    return true;
}

bool ParameterHandler::upgrade() {
    cout << "\33[1;32mUpgrading packages\033[0m" << endl;

    Database db;
    db.openfile();

    for (Package pkg: db.listInstalledPkgs()) {
        for (Repository repo: db.listRepositories()) {
            if (!repo.downloadRepo()) {
                cout << endl << "\033[1;31mERROR \033[0m\033[1;37m- Cannot download repo, please try again later! \033[0m" << endl;
                return false;
            }
            for (Package newpkg: repo.getAllPackages()) {
                if (newpkg.getname() == pkg.getname()) {
                    if (newpkg.getversion() == pkg.getversion()) {
                        cout << "\33[1;37m  " << pkg.getname() << "\033[0m is already up to date." << endl;
                    } else {
                        cout << "\33[1;37m  Upgrading package \033[0m" << pkg.getname() << "..." << endl;
                        db.uninstall(&pkg);
                        db.install(&newpkg);
                    }
                }
            }
        }
        cout << endl;
    }

    return true;
}

bool ParameterHandler::help() {
    cout << "\33[1;37m  iinstall <PACKAGE>                       ->\033[0m Install the selected package(s)" << endl;
    cout << "\33[1;37m  iinstall -i/--install-local <NAME> <FILE>->\033[0m Install package from a local file" << endl;
    cout << "\33[1;37m  iinstall -r/--remove <PACKAGE>           ->\033[0m Remove the selected package(s)" << endl;
    cout << "\33[1;37m  iinstall -u/--upgrade                    ->\033[0m Upgrade the installed packages" << endl;
    cout << "\33[1;37m  iinstall -c/--compile <NAME> <SOURCE>    ->\033[0m Create a package" << endl;
    cout << "\33[1;37m  iinstall -C/--createrepo <PACKAGES>      ->\033[0m Create a repository" << endl;
    cout << "\33[1;37m  iinstall -s/--search <PACKAGE>           ->\033[0m Search for an expression in the repos" << endl;
    cout << "\33[1;37m  iinstall -l/--list                       ->\033[0m List currently installed packages" << endl;
    cout << "\33[1;37m  iinstall -h/--help                       ->\033[0m Show the iinstall-help" << endl;

    return true;
}

bool ParameterHandler::list() {
    cout << "\33[1;32mCurrently installed packages:\033[0m" << endl;

    Database db;
    db.openfile();

    for (Package pkg: db.listInstalledPkgs()) {
        cout << "\33[1;37m  " << pkg.getname() << "\033[0m " << pkg.getversion() << endl;
    }

    return true;
}

bool ParameterHandler::search(int argc, char* argv[]) {
    if (argc != 3) {
        cout << "\033[1;31mERROR \033[0m\033[1;32m- Usage: \033[0m" << endl;
        ParameterHandler::help();
        return 1;
    }

    cout << "\33[1;32mSearch results:\033[0m" << endl;

    Database db;
    db.openfile();

    for (Repository repo: db.listRepositories()) {
        if (!repo.downloadRepo()) {
            cout << endl << "\033[1;31mERROR \033[0m\033[1;37m- Cannot download repo, please try again later! \033[0m" << endl;
            return false;
        }
        for (Package pkg: repo.getAllPackages()) {
            if (pkg.getname().find(argv[2]) != string::npos) {
                cout << "\33[1;37m  " << pkg.getname() << "\033[0m " << pkg.getversion() << endl;
                cout << "      " << pkg.getdescription() << ", \33[1;37m" << pkg.getlicense() << "\033[0m" << endl << endl;
            }
        }
    }

    return true;
}
